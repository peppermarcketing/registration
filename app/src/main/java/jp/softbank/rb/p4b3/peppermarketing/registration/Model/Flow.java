package jp.softbank.rb.p4b3.peppermarketing.registration.Model;

public class Flow {
    private int type;
    private String code;

    private String roboappMessage1; // フロー開始時のセリフ
    private String roboappMessage2; // フロー開始時の表示文言
    private String roboappMessage3; // フロー終了時のセリフ
    private String roboappMessage4; // フロー終了時の表示文言

    public void setType(int type) {
        this.type = type;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getType() {
        return type;
    }

    public String getCode() {
        return code;
    }

    public void setRoboappMessage1(String roboappMessage1) {
        this.roboappMessage1 = roboappMessage1;
    }

    public void setRoboappMessage2(String roboappMessage2) {
        this.roboappMessage2 = roboappMessage2;
    }

    public void setRoboappMessage3(String roboappMessage3) {
        this.roboappMessage3 = roboappMessage3;
    }

    public void setRoboappMessage4(String roboappMessage4) {
        this.roboappMessage4 = roboappMessage4;
    }

    public String getRoboappMessage1() {
        return roboappMessage1;
    }

    public String getRoboappMessage2() {
        return roboappMessage2;
    }

    public String getRoboappMessage3() {
        return roboappMessage3;
    }

    public String getRoboappMessage4() {
        return roboappMessage4;
    }
}
