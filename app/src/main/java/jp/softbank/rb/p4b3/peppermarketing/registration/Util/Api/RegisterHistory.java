package jp.softbank.rb.p4b3.peppermarketing.registration.Util.Api;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.registration.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Logutil;

public class RegisterHistory {

    private final String TAG = this.getClass().getSimpleName();

    private String accessToken;
    private int memberId;
    private String flowCode;
    private String pepperName;
    private int visitType;
    private RegisterHistoryCallback callback;

    public RegisterHistory(String accessToken, int memberId, String flowCode, String pepperName, int visitType, RegisterHistoryCallback callback) {
        this.accessToken = accessToken;
        this.memberId = memberId;
        this.flowCode = flowCode;
        this.pepperName = pepperName;
        this.visitType = visitType;
        this.callback = callback;
    }

    // 行動履歴登録処理
    public void excute() {

        Logutil.log("API_開始_行動履歴登録処理");

        // 行動履歴登録
        final String url = "https://rap-vm.softbankrobotics.com/portal/pmkt/api3.php/member/visit";


        // POSTのヘッダー情報付加
        ArrayList<HttpTask.Param> headers = new ArrayList<>();
        headers.add(new HttpTask.Param("Content-Type", "application/x-www-form-urlencoded"));

        // POSTパラメータ
        String param = "access_token=" + accessToken + "&" +
                "member_id=" + String.valueOf(memberId) + "&" +
                "flow_code=" + flowCode + "&" +
                "pepper_name=" + pepperName + "&" +
                "visit_type=" + String.valueOf(visitType);

        // 引数(URL,パラメータ,ヘッダー)
        final HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                param,
                headers
        );

        // 通信前処理、通信後処理を設定
        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    Logutil.log("API_終了_行動履歴登録処理");

                    // 通信成功
                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    // パース
                    boolean isSuccess = isSuccess(resultJson);
                    callback.onFinish(response, isSuccess);

                } else {

                    Logutil.log("API_エラー_行動履歴登録処理");

                    Log.e(TAG, response.errorMessage);
                    callback.onFinish(response, false);
                }
            }

            @Override
            public void onPre() { }
        }, true).execute(request);
    }

    // jsonから成功したかどうかを取得
    private static boolean isSuccess(String strJson) {

        boolean success = false;

        try {
            JSONObject rootJson = new JSONObject(strJson);
            // keyで情報を取得
            success = rootJson.getBoolean("success");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return success;
    }

    static public class VisitType {
        public static final int TEMPORARY_REGISTRATION = 1;
        public static final int GENERALIZE_MEMBER = 2;
        public static final int COUPON_RECEIPT = 3;
        public static final int MESSAGE_RECEIPT = 4;
        public static final int REWARD_RECEIPT_FAILURE = 6;
    }

    public interface RegisterHistoryCallback {
        void onFinish(HttpTask.HttpResponse response, boolean isSuccess);
    }
}
