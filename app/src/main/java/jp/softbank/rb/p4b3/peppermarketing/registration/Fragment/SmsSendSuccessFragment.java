package jp.softbank.rb.p4b3.peppermarketing.registration.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import jp.softbank.rb.p4b3.peppermarketing.registration.Fragment.MarketingFragment;
import jp.softbank.rb.p4b3.peppermarketing.registration.R;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.PepperUtil;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.PepperMemory;

public class SmsSendSuccessFragment extends MarketingFragment {

    TextView txtMessage;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_sms_send_success, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtMessage = view.findViewById(R.id.txt_sms_success);
        txtMessage.setText(PepperMemory.flow.getRoboappMessage4());

        sayPepper();
    }

    // 発話
    private void sayPepper() {

        List<String> texts = Arrays.asList(
                getString(R.string.sms_send_success_b_1),
                getString(R.string.sms_send_success_b_2)
        );

        String speechText = PepperMemory.flow.getRoboappMessage3() + texts.get((new Random().nextInt(texts.size())));

        // 発話中なら発話中断
        sayCancel();

        say = new Say(qiContext, speechText, true, new ActionCallback() {
            @Override
            public void onSuccess() {
                // アプリ終了
                PepperUtil.finishApplication(activity);
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }
}
