package jp.softbank.rb.p4b3.peppermarketing.registration.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.aldebaran.qi.sdk.QiContext;

import jp.softbank.rb.p4b3.peppermarketing.registration.R;
import jp.softbank.rb.p4b3.peppermarketing.registration.RegistrationActivity;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.PepperUtil;
import jp.softbank.rb.p4b3.peppermarketing.registration.View.PepperButton;

public class MarketingFragment extends Fragment {

    public String TAG;

    // qiContext
    public QiContext qiContext;

    // Activity
    public RegistrationActivity activity;

    // Handler
    public Handler handler;

    // 共通UI
    public PepperButton btnFinish;
    public ConstraintLayout layoutRoot;
    public View.OnClickListener buttonFinishListener;

    //
    public Say say;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        TAG = this.getClass().getSimpleName();
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        this.activity = (RegistrationActivity) context;
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        handler = new Handler();

        // 遷移元Activityから物を貰う
        qiContext = activity.qiContext;

        btnFinish = view.findViewById(R.id.btn_finish);
        buttonFinishListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (layoutRoot != null) {
                    allButtonSetActive(layoutRoot, false);
                }

                sayCancel();

                say = new Say(qiContext, getString(R.string.app_finish), true, new ActionCallback() {
                    @Override
                    public void onSuccess() {
                        PepperUtil.finishApplication(activity);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        this.onSuccess();
                    }

                    @Override
                    public void onCancel() {
                        this.onSuccess();
                    }
                });

                say.execute();
            }
        };

        layoutRoot = view.findViewById(R.id.layout_root);
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");

        super.onStop();

        sayCancel();

        if (activity.timeout != null) {
            activity.timeout.stop();
            activity.timeout = null;
        }
    }

    public void allButtonSetActive(ConstraintLayout layout, boolean isActive) {
        for (int i = 0; i < layout.getChildCount(); i++) {
            if (layout.getChildAt(i) instanceof PepperButton) {
                ((PepperButton) layout.getChildAt(i)).setActive(isActive);
            }
            if (layout.getChildAt(i) instanceof ConstraintLayout) {
                allButtonSetActive((ConstraintLayout) layout.getChildAt(i), isActive);
            }
        }
    }

    public void timeout() {

        // 発話中なら発話中断
        sayCancel();

        say = new Say(qiContext, getString(R.string.timeout), true, new ActionCallback() {
            @Override
            public void onSuccess() {
                PepperUtil.finishApplication(activity);
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }

    public void sayCancel() {
        // 発話中なら発話中断
        if (say != null && say.isWorking()) {
            say.cancel();
        }
    }
}
