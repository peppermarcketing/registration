package jp.softbank.rb.p4b3.peppermarketing.registration.Util;

public class PackageNames {
    public static final String REGISTRATION = "jp.softbank.rb.p4b3.peppermarketing.registration";
    public static final String STOREPROMOTION = "jp.softbank.rb.p4b3.peppermarketing.storepromotion";
    public static final String SETTING = "jp.softbank.rb.p4b3.peppermarketing.setting";

    public static final String SETTING_P4B3 = "jp.softbank.rb.p4b3.p4b3settings";
}
