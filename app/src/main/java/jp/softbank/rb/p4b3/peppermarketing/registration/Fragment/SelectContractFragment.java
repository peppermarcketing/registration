package jp.softbank.rb.p4b3.peppermarketing.registration.Fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import jp.softbank.rb.p4b3.peppermarketing.registration.R;
import jp.softbank.rb.p4b3.peppermarketing.registration.View.PepperButton;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.PepperMemory;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Timeout;

public class SelectContractFragment extends MarketingFragment {

    PepperButton btnContract_01;
    PepperButton btnContract_02;
    PepperButton btnContractOk;
    TextView txtContractTextSbr;
    TextView txtContractTextUser;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_select_kiyaku_bk, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI設定
        btnContract_01 = view.findViewById(R.id.btn_select_kiyaku_1);
        btnContract_02 = view.findViewById(R.id.btn_select_kiyaku_2);
        btnContractOk = view.findViewById(R.id.btn_select_kiyaku_ok);
        txtContractTextSbr = view.findViewById(R.id.contract_text_sbr);
        txtContractTextUser = view.findViewById(R.id.contract_text_user);

        // 利用規約文セット
        txtContractTextSbr.setText(PepperMemory.term.getTermSbr());
        txtContractTextUser.setText(PepperMemory.term.getTermUser());

        // クリック時の処理
        btnContract_01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnContract_01.setActive(false);
                btnContract_02.setActive(false);
                activity.soundPool.play(activity.seTap, 1.0f, 1.0f, 0, 0, 1.0f);
                ContractDialogFragment dialogFragment = new ContractDialogFragment();
                dialogFragment.setTitle("ソフトバンクロボティクス株式会社利用規約");
                dialogFragment.setText(PepperMemory.term.getTermSbr());
                dialogFragment.setDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        activity.soundPool.play(activity.seCancel, 1.0f, 1.0f, 0, 0, 1.0f);
                        btnContract_01.setActive(true);
                        btnContract_02.setActive(true);

                        // タイムアウトリセット
                        activity.timeout.resetCount();
                    }
                });
                if (getFragmentManager() != null) {
                    dialogFragment.show(getFragmentManager(), TAG);
                }

                // タイムアウトリセット
                activity.timeout.resetCount();
            }
        });
        btnContract_02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnContract_01.setActive(false);
                btnContract_02.setActive(false);
                activity.soundPool.play(activity.seTap, 1.0f, 1.0f, 0, 0, 1.0f);
                ContractDialogFragment dialogFragment = new ContractDialogFragment();
                dialogFragment.setTitle("サービス提供企業利用規約");
                dialogFragment.setText(PepperMemory.term.getTermUser());
                dialogFragment.setDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        activity.soundPool.play(activity.seCancel, 1.0f, 1.0f, 0, 0, 1.0f);
                        btnContract_01.setActive(true);
                        btnContract_02.setActive(true);

                        // タイムアウトリセット
                        activity.timeout.resetCount();
                    }
                });
                if (getFragmentManager() != null) {
                    dialogFragment.show(getFragmentManager(), TAG);
                }

                // タイムアウトリセット
                activity.timeout.resetCount();
            }
        });
        btnContractOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // タイムアウトリセット
                activity.timeout.resetCount();

                activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);

                allButtonSetActive(layoutRoot, false);
                activity.changeFragment(new EnterPhoneNumberFragment());
            }
        });


        // 終了ボタン処理
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);
                buttonFinishListener.onClick(view);
            }
        });

        // タイムアウト設定
        activity.timeout = new Timeout(30);
        activity.timeout.setTimeoutListener(new Timeout.TimeoutListener() {
            @Override
            public void onTimeout() {
                timeout();
            }
        });

        // 発話
        sayPepper();
    }

    //発話
    private void sayPepper() {

        // 発話中なら発話中断
        sayCancel();

        say = new Say(qiContext, getString(R.string.choose_a_contract), true, new ActionCallback() {
            @Override
            public void onSuccess() {
                // タイムアウトスタート
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        activity.timeout.start();
                    }
                });
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                //this.onSuccess();
            }
        });

        say.execute();
    }
}
