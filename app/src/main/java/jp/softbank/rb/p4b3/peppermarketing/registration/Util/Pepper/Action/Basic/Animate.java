package jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.Basic;

import android.util.Log;

import com.aldebaran.qi.Consumer;
import com.aldebaran.qi.Future;
import com.aldebaran.qi.sdk.QiContext;
import com.aldebaran.qi.sdk.builder.AnimateBuilder;
import com.aldebaran.qi.sdk.builder.AnimationBuilder;
import com.aldebaran.qi.sdk.object.actuation.Animation;

import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.ActionCallback;

public class Animate {

    private final String TAG = "Animate";

    private QiContext qiContext;
    private int motionId;
    private ActionCallback callback;

    private Future<Void> fAnimate;

    boolean isWorking;

    public Animate (QiContext qiContext, int motionId, ActionCallback callback) {
        this.qiContext = qiContext;
        this.motionId = motionId;
        this.callback = callback;
    }

    public void execute() {

        isWorking = true;

        AnimationBuilder.with(qiContext)
                .withResources(motionId)
                .buildAsync()
                .thenConsume(new Consumer<Future<Animation>>() {
                    @Override
                    public void consume(Future<Animation> animationFuture) throws Throwable {

                        fAnimate = AnimateBuilder.with(qiContext)
                                .withAnimation(animationFuture.get())
                                .build()
                                .async()
                                .run();

                        Log.d(TAG, "start");

                        fAnimate.thenConsume(new Consumer<Future<Void>>() {
                            @Override
                            public void consume(Future<Void> future) {

                                if (future.isDone()) {
                                    Log.d(TAG, "isDone");
                                    isWorking = false;

                                    if (callback == null) {
                                        Log.d(TAG, "callback is null");
                                        return;
                                    }

                                    if (future.isSuccess()) {
                                        Log.d(TAG, "isSuccess");
                                        callback.onSuccess();

                                    } else if (future.hasError()) {
                                        Log.d(TAG, "hasError");
                                        callback.onError(future.getErrorMessage());

                                    } else if (future.isCancelled()) {
                                        Log.d(TAG, "isCancelled");
                                        callback.onCancel();
                                    }
                                }
                            }
                        });
                    }
                });
    }

    public void cancel() {
        fAnimate.cancel(true);
    }

    public boolean isWorking() {
        return isWorking;
    }
}
