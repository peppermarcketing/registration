package jp.softbank.rb.p4b3.peppermarketing.registration.Util.Api;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.registration.Model.Reward;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Logutil;

public class GiveReward {

    private final String TAG = this.getClass().getSimpleName();

    private String accessToken;
    private int memberId;
    private int rewardType;
    private int rewardId;
    private String flowCode;
    private String pepperName;
    private GiveRewardCallback callback;

    public GiveReward(String accessToken, int memberId, int rewardType, int rewardId, String flowCode, String pepperName, GiveRewardCallback callback) {
        this.accessToken = accessToken;
        this.memberId = memberId;
        this.rewardType = rewardType;
        this.rewardId = rewardId;
        this.flowCode = flowCode;
        this.pepperName = pepperName;
        this.callback = callback;
    }

    // リワード付与処理
    public void excute() {

        Logutil.log("API_開始_リワード付与処理");

        // リワード付与
        final String url = "https://rap-vm.softbankrobotics.com/portal/pmkt/api3.php/reward/give";

        // POSTのヘッダー情報付加
        ArrayList<HttpTask.Param> headers = new ArrayList<>();
        headers.add(new HttpTask.Param("Content-Type", "application/x-www-form-urlencoded"));

        // POSTパラメータ
        String param = "access_token=" + accessToken + "&" +
                "member_id=" + String.valueOf(memberId) + "&" +
                "reward_type=" + String.valueOf(rewardType) + "&" +
                "reward_id=" + String.valueOf(rewardId) + "&" +
                "flow_code=" + flowCode + "&" +
                "pepper_name=" + pepperName;

        // 引数(URL,パラメータ,ヘッダー)
        final HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                param,
                headers
        );

        // 通信前処理、通信後処理を設定
        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    Logutil.log("API_終了_リワード付与処理");

                    // 通信成功
                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    // パース
                    String message = getMessage(resultJson, rewardType);
                    callback.onFinish(response, message);

                } else {

                    Logutil.log("API_エラー_リワード付与処理");

                    Log.e(TAG, response.errorMessage);
                    // 何らかのエラーがあった場合
                    callback.onFinish(response, null);
                }
            }

            @Override
            public void onPre() { }
        }, true).execute(request);
    }

    // jsonからフロー情報を取得
    private String getMessage(String strJson, int rewardType) {

        String message = "";

        try {
            JSONObject rootJson = new JSONObject(strJson);
            // keyで情報を取得 リワードの種類によってキーが異なる
            if (rewardType == Reward.RewardType.COUPON || rewardType == Reward.RewardType.LOTTERY) {
                message = rootJson.getString("reward_shorturl");
            } else if (rewardType == Reward.RewardType.MESSAGE) {
                message = rootJson.getString("sms_message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return message;
    }

    public interface GiveRewardCallback {
        void onFinish(HttpTask.HttpResponse response, String message);
    }
}
