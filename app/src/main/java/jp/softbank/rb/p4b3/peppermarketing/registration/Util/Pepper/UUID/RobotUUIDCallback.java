package jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.UUID;

import com.aldebaran.robotservice.Callback;

public class RobotUUIDCallback extends Callback.Stub {

    private RobotServiceHandler handler;

    public RobotUUIDCallback(RobotServiceHandler handler) {
        this.handler = handler;
    }

    @Override
    public void onSuccess(String robotUUID) {
        handler.sendRobotUUID(robotUUID);
    }

    @Override
    public void onError(int i, String s) {

    }
}