package jp.softbank.rb.p4b3.peppermarketing.registration.Util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class HttpTask extends AsyncTask<HttpTask.HttpRequest, Integer, HttpTask.HttpResponse> {

    HttpListener listener;
    boolean isPost;

    public HttpTask(HttpListener listener, boolean isPost) {
        this.listener = listener;
        this.isPost = isPost;
    }


    @Override
    protected HttpResponse doInBackground(HttpRequest... httpRequests) {

        if (httpRequests.length == 0) {
            return null;
        }

        HttpRequest request = httpRequests[0];
        HttpResponse response = new HttpResponse();

        int readTimeout = request.readTimeout;
        int connectTimeout = request.connectTimeout;

        HttpURLConnection connection = null;
        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {
            // URLとheaderの設定
            URL url = new URL(request.url);
            ArrayList<Param> headers = request.headers;
            // connection
            connection = (HttpURLConnection)url.openConnection();
            // request POST
            String methd = isPost ? "POST" : "GET";
            connection.setRequestMethod(methd);
            if (isPost) {
                // no Redirects
                connection.setInstanceFollowRedirects(false);
                // データを書き込む
                connection.setDoOutput(true);
            }
            // 時間制限
            connection.setReadTimeout(readTimeout);
            connection.setConnectTimeout(connectTimeout);
            //header
            if (headers != null) {
                for (Param header : headers) {
                    connection.setRequestProperty(header.key, header.value);
                }
            }
            // 接続
            connection.connect();

            if (isPost) {
                // POSTデータ送信処理
                outputStream = connection.getOutputStream();
                outputStream.write(request.param.getBytes("UTF-8") );
                outputStream.flush();
            }

            final int status = connection.getResponseCode();
            response.status = status;
            if (status == HttpURLConnection.HTTP_OK) {
                // レスポンスを受け取る処理等

                // テキストを取得する
                inputStream = connection.getInputStream();

                response.isSuccess = true;
                response.result = readAll(inputStream);
            }
            else{
                response.isSuccess = false;
                try {
                    inputStream = connection.getInputStream();
                } catch (FileNotFoundException e) {
                    inputStream = connection.getErrorStream();
                } finally {
                    if (inputStream != null) {
                        response.errorMessage = byte2string(readAll(inputStream));
                    }
                }
            }

        } catch (Exception e) {
            response.isSuccess = false;
            response.errorMessage = e.toString();
        }

        return response;
    }

    @Override
    protected void onPreExecute() {
        listener.onPre();
    }

    @Override
    protected void onPostExecute(HttpResponse httpResponse){
        super.onPostExecute(httpResponse);
        listener.onFinish(httpResponse);
    }


    private byte[] readAll(InputStream inputStream) throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        byte [] buffer = new byte[1024];
        while(true) {
            int len = inputStream.read(buffer);
            if(len < 0) {
                break;
            }
            bout.write(buffer, 0, len);
        }
        return bout.toByteArray();
    }

    static public String byte2string(byte[] bytes) {
        String str = new String(bytes);
        return str;
    }

    static public Bitmap byte2bitmup(byte[] bytes) {

        Bitmap bmp = null;
        if (bytes != null) {
            bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }
        return bmp;
    }

    public static class HttpRequest {
        public String url;
        public String param;
        public ArrayList<Param> headers;
        public int readTimeout = 10000;
        public int connectTimeout = 20000;

        public HttpRequest(String url, String param, ArrayList<Param> headers, int readTimeout, int connectTimeout) {
            this.url = url;
            this.param = param;
            this.headers = headers;
            this.readTimeout = readTimeout;
            this.connectTimeout = connectTimeout;
        }

        public HttpRequest(String url, String param, ArrayList<Param> headers) {
            this.url = url;
            this.param = param;
            this.headers = headers;
        }

        public HttpRequest(String url, String param) {
            this.url = url;
            this.param = param;
        }
    }

    public static class Param {
        public String key;
        public String value;

        public Param(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    public class HttpResponse {
        public int status;
        public boolean isSuccess;
        public byte[] result = null;
        public String errorMessage = null;
    }

    public interface HttpListener {
        void onFinish(HttpResponse response); // 通信終了時処理
        void onPre(); // 通信開始前処理
    }
}
