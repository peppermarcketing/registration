package jp.softbank.rb.p4b3.peppermarketing.registration.Util;

import android.app.Application;
import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.os.Build;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public class Logutil extends Application {

    private static final String FILE_NAME = "log.txt";
    private static final String FILE_NAME_OLD = "log_old.txt";
    private static final int LOGFILE_SIZE = 1000000;

    static public void log (String text) {

        // ファイルサイズ取得
        int size = 0;
        try {
            FileInputStream file = getContext().openFileInput(FILE_NAME);
            size = file.available();
            file.close();
        } catch(IOException e) {
            e.printStackTrace();
        }

        // ファイルサイズチェック 1MB超えないように、閾値は少し少なめ
        if (size > LOGFILE_SIZE) {
            // oldファイル削除
            getContext().deleteFile(FILE_NAME_OLD);

            // メインのファイル内容を取得
            StringBuilder logs = new StringBuilder();
            try {
                FileInputStream inputStream = getContext().openFileInput(FILE_NAME);
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String tmp;
                while (true) {
                    tmp = reader.readLine();
                    if (tmp == null) {
                        break;
                    } else {
                        logs.append(tmp).append("\n");
                    }
                }
                reader.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

            // oldファイル作成
            try (FileOutputStream fileOutputstream = getContext().openFileOutput(FILE_NAME_OLD, Context.MODE_APPEND)) {

                fileOutputstream.write(logs.toString().getBytes());

            } catch (IOException e) {
                e.printStackTrace();
            }

            //メインのファイル削除
            getContext().deleteFile(FILE_NAME);
        }

        String strDatetime;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        // 現在の時刻を取得
            Date date = new Date();
            // 表示形式を設定
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy'年'MM'月'dd'日'　kk'時'mm'分'ss'秒'");
            strDatetime = sdf.format(date);
        } else {
            Date todayDate = new Date();

            final String YYYY = String.valueOf(todayDate.getYear());
            final String MM = String.valueOf(todayDate.getMonth());
            final String DD = String.valueOf(todayDate.getDay());
            final String HH = String.valueOf(todayDate.getHours());
            final String mm = String.valueOf(todayDate.getMinutes());
            final String ss = String.valueOf(todayDate.getSeconds());

            strDatetime = YYYY + "/" + MM + "/" + DD + " " + HH + ":" + mm + ":" + ss;
        }

        String logText = text + " " + strDatetime + "\n";

        // try-with-resources
        try (FileOutputStream fileOutputstream = getContext().openFileOutput(FILE_NAME, Context.MODE_APPEND)) {

            fileOutputstream.write(logText.getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 以下コンテキストを取得するためのコード

    private static Application sApplication;

    private static Application getApplication() {
        return sApplication;
    }

    private static Context getContext() {
        return getApplication().getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sApplication = this;
    }
}
