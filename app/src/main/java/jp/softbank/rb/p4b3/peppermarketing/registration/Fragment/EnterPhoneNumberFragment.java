package jp.softbank.rb.p4b3.peppermarketing.registration.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import jp.softbank.rb.p4b3.peppermarketing.registration.Model.Reward;
import jp.softbank.rb.p4b3.peppermarketing.registration.R;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Api.GiveReward;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Api.SearchMember;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Api.SendMessage;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Api.TemporalRegister;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.Advance.SayAnimate;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.PepperUtil;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.PepperMemory;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Timeout;
import jp.softbank.rb.p4b3.peppermarketing.registration.View.AutoResizeTextView;
import jp.softbank.rb.p4b3.peppermarketing.registration.View.PepperButton;


public class EnterPhoneNumberFragment extends MarketingFragment {

    AutoResizeTextView txtPhoneNumber;

    PepperButton btn1;
    PepperButton btn2;
    PepperButton btn3;
    PepperButton btn4;
    PepperButton btn5;
    PepperButton btn6;
    PepperButton btn7;
    PepperButton btn8;
    PepperButton btn9;
    PepperButton btn0;

    PepperButton btnBack;
    PepperButton btnDone;
    PepperButton btnContract;

    ImageView imgNumberMisstake;

    TextView txtMessage;

    //入力されている電話番号
    String phoneNumber = "";

    // smsで送信するメッセージ
    String smsText = "";

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_enter_phone_number, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // button init
        btn0 = view.findViewById(R.id.button_phone_0);
        btn1 = view.findViewById(R.id.button_phone_1);
        btn2 = view.findViewById(R.id.button_phone_2);
        btn3 = view.findViewById(R.id.button_phone_3);
        btn4 = view.findViewById(R.id.button_phone_4);
        btn5 = view.findViewById(R.id.button_phone_5);
        btn6 = view.findViewById(R.id.button_phone_6);
        btn7 = view.findViewById(R.id.button_phone_7);
        btn8 = view.findViewById(R.id.button_phone_8);
        btn9 = view.findViewById(R.id.button_phone_9);
        btnBack = view.findViewById(R.id.button_phone_del);
        btnDone = view.findViewById(R.id.marketing_button_done);
        btnContract = view.findViewById(R.id.kiyaku);
        // set cliclListeneer
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("0");
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("1");
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("2");
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("3");
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("4");
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("5");
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("6");
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("7");
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("8");
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("9");
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickDone();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("");
            }
        });
        btnContract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickContract();
            }
        });
        // init active
        btnBack.setActive(false);
        btnDone.setActive(false);

        // その他UI
        txtPhoneNumber = view.findViewById(R.id.txt_phone_number);
        txtPhoneNumber.setText("");
        imgNumberMisstake = view.findViewById(R.id.img_number_misstake);
        imgNumberMisstake.setVisibility(View.GONE);
        txtMessage = view.findViewById(R.id.lbl_please_enter);
        txtMessage.setText(PepperMemory.flow.getRoboappMessage2());


        // 終了ボタン処理
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);
                buttonFinishListener.onClick(view);
            }
        });

        // タイムアウト設定
        if (activity.timeOfSend <= 0) {
            activity.timeout = new Timeout(180);
        } else {
            activity.timeout = new Timeout(60);
        }
        activity.timeout.setTimeoutListener(new Timeout.TimeoutListener() {
            @Override
            public void onTimeout() {
                timeout();
            }
        });

        // 発話
        sayGreeting();
    }

    // 発話
    private void sayGreeting() {

        List<String> texts = Arrays.asList(
                getString(R.string.please_enter_phone_number_a_1),
                getString(R.string.please_enter_phone_number_a_2),
                getString(R.string.please_enter_phone_number_a_3)
        );

        String speechText = texts.get((new Random().nextInt(texts.size()))) + PepperMemory.flow.getRoboappMessage1();

        // 発話中なら発話中断
        sayCancel();

        say = new Say(qiContext, speechText, true, new ActionCallback() {
            @Override
            public void onSuccess() {

                // タイムアウトスタート
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        activity.timeout.start();
                    }
                });
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }

    // 規約ボタンクリック時
    private void clickContract() {

        // タイムアウトリセット
        activity.timeout.resetCount();

        activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);

        // ボタン非活性
        allButtonSetActive(layoutRoot, false);

        // 発話中なら発話中断
        sayCancel();

        say = new Say(qiContext, getString(R.string.click_contract), false, new ActionCallback() {
            @Override
            public void onSuccess() {
                // 規約画面へ遷移
                activity.changeFragment(new SelectContractFragment());
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }

    // 次へボタンクリック時
    private void clickDone() {

        // タイムアウトリセット
        activity.timeout.resetCount();

        // ボタン非活性
        allButtonSetActive(layoutRoot, false);

        activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);

        // 桁数チェック
        if (phoneNumber.length() != 11) {

            // 発話中なら発話中断
            sayCancel();

            say = new Say(qiContext, getString(R.string.phone_number_not_11), true, new ActionCallback() {
                @Override
                public void onSuccess() {
                    allButtonSetActive(layoutRoot, true);
                    btnDone.setActive(false);
                }

                @Override
                public void onError(String errorMessage) {
                    this.onSuccess();
                }

                @Override
                public void onCancel() {
                    this.onSuccess();
                }
            });

            say.execute();

        } else {
            // 形式チェック
            // 先頭3文字をチェック
            String top = phoneNumber.substring(0, 3);
            if (!top.equals("090") && !top.equals("080") && !top.equals("070")) {

                allButtonSetActive(layoutRoot, true);
                changeButtonsState(false);

                showErrorPhoneNumber();
                return;
            }

            // タイムアウト終了
            activity.timeout.stop();

            // 御礼の挨拶
            sayCancel();
            int motionId = PepperUtil.convertResourceID(qiContext.getApplicationContext(), "bow_a001");
            say = new SayAnimate(qiContext, getString(R.string.entered_number), motionId, new ActionCallback() {
                @Override
                public void onSuccess() {
                    // APIリクエスト 会員検索
                    searchMember();
                }

                @Override
                public void onError(String errorMessage) {
                    onSuccess();
                }

                @Override
                public void onCancel() {
                    onSuccess();
                }
            });
            say.execute();
            //sendMessage(); // テスト用
        }
    }

    // 携帯番号更新
    private void enterPhoneNumber(String number) {

        // タイムアウトリセット
        activity.timeout.resetCount();

        // 1文字削除
        if (number.equals("")) {

            activity.soundPool.play(activity.seCancel, 1.0f, 1.0f, 0, 0, 1.0f);

            // 消すので最低1文字は欲しい
            if (phoneNumber.length() < 1) {
                return;
            }

            // 一文字削除（文字列切り抜き）
            phoneNumber = phoneNumber.substring(0, phoneNumber.length() - 1);
        }

        else {

            // 携帯電話番号は11桁
            if (phoneNumber.length() >= 11) {
                return;
            }

            activity.soundPool.play(activity.seTap, 1.0f, 1.0f, 0, 0, 1.0f);

            phoneNumber += number;
        }

        // ****で表示する
        StringBuilder secret = new StringBuilder();
        for (int i = 0; i < phoneNumber.length(); i++) {
            secret.append("*");
        }

        // 文字数チェック
        // 11文字なら数字を非活性, 次へを活性化(発話中であった場合非活性)
        if (phoneNumber.length() >= 11) {
            changeButtonsState(false);
            btnDone.setActive(true);

        } else {
            changeButtonsState(true);
            btnDone.setActive(false);
        }
        // 1文字も無ければバックを非活性
        if (phoneNumber.length() <= 0) {
            btnBack.setActive(false);
        } else {
            btnBack.setActive(true);
        }

        // 電話番号入力チェック
        // 3文字になった時（だけでいいのか。。。）
        if (!number.equals("")) {
            if (phoneNumber.length() == 3) {
                // 先頭3文字をチェック
                String top = phoneNumber.substring(0, 3);
                if (top.equals("090") || top.equals("080") || top.equals("070")) {
                    Log.d(TAG, "phone number ok");
                } else {
                    showErrorPhoneNumber();
                }
            }
        }

        // *** を表示
        txtPhoneNumber.setText(secret.toString());
    }

    // 電話番号エラーを出す
    private void showErrorPhoneNumber() {
        imgNumberMisstake.setVisibility(View.VISIBLE);
        // 5秒で消す
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                imgNumberMisstake.setVisibility(View.GONE);
            }
        },5000);

        List<String> texts = Arrays.asList(
                getString(R.string.phone_number_misstale_1),
                getString(R.string.phone_number_misstale_2),
                getString(R.string.phone_number_misstale_3)
        );

        // 発話中なら発話中断
        if (say != null && say.isWorking()) {
            say.cancel();
        }

        say = new Say(qiContext, texts.get((new Random()).nextInt(texts.size())), true, null);
        say.execute();
    }

    // ボタン状態変化
    private void changeButtonsState(boolean isActive) {
        btn0.setActive(isActive);
        btn1.setActive(isActive);
        btn2.setActive(isActive);
        btn3.setActive(isActive);
        btn4.setActive(isActive);
        btn5.setActive(isActive);
        btn6.setActive(isActive);
        btn7.setActive(isActive);
        btn8.setActive(isActive);
        btn9.setActive(isActive);
    }

    // 会員検索
    private void searchMember() {

        String accessToken = PepperMemory.accessToken;

        new SearchMember(accessToken, SearchMember.SearchType.TEL, phoneNumber, new SearchMember.SearchMemberCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, int memberId, String memberUrl) {

                // 通信成功・失敗確認
                if (response.isSuccess) {
                    // 通信成功 = 会員情報あり
                    activity.changeFragment(new RegisteredFragment());

                } else {

                    if (response.status == 404) {
                        Log.d(TAG, "会員情報なし: " + phoneNumber);
                    } else if (response.status == 415) {
                        Log.d(TAG, "仮登録、退会済、基本設定未了のいずれか");
                    }

                    // 通信失敗 = 会員情報なし等
                    // 会員情報が無かったら404
                    switch (response.status) {
                        case 404:
                        case 415:
                            // 仮登録処理
                            temporalRegister();
                            break;
                        default:
                            // APIエラー
                            activity.changeFragment(new ErrorFragment());
                            break;

                    }
                }
            }
        }).execute();
    }

    // 仮登録処理
    private void temporalRegister() {

        String accessToken = PepperMemory.accessToken;
        String flowCode = PepperMemory.flow.getCode();
        int flowType = PepperMemory.flow.getType();
        final String pepperName = PepperMemory.pepperName;

        new TemporalRegister(accessToken, flowCode, flowType, pepperName, phoneNumber, new TemporalRegister.TemporalRegisterCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, int memberId, String message) {

                // 通信成功・失敗確認
                if (response.isSuccess) {

                    PepperMemory.memberId = memberId;
                    smsText = pepperName + " からのテスト送信（登録用）\n\n" + message;

                    // リワード付与
                    giveReword();

                } else {
                    // APIエラー
                    activity.changeFragment(new ErrorFragment());
                }
            }
        }).excute();
    }

    // 行動履歴登録処理 今回は仮登録で
//    private void registerHistory() {
//
//        String accessToken = PepperMemory.accessToken;
//        int memberId = PepperMemory.memberId;
//        String flowCode = PepperMemory.flow.getCode();
//        String pepperName = PepperMemory.pepperName;
//        int visitType = RegisterHistory.VisitType.TEMPORARY_REGISTRATION;
//
//        new RegisterHistory(accessToken, memberId, flowCode, pepperName, visitType, new RegisterHistory.RegisterHistoryCallback() {
//            @Override
//            public void onFinish(HttpTask.HttpResponse response, boolean isSuccess) {
//
//                // 通信成功・失敗確認
//                if (response.isSuccess) {
//
//                    if (isSuccess) {
//                        // リワード付与
//                        giveReword();
//                    }
//                } else {
//                    // APIエラー
//                    fragmentInterface.changeFragment(new ErrorFragment());
//                }
//            }
//        }).excute();
//    }

    // リワード付与処理
    private void giveReword() {

        Reward reward = new Reward();
        if (PepperMemory.rewards.size() > 0) {
            reward = PepperMemory.rewards.get(0);
        }

        String accessToken = PepperMemory.accessToken;
        int memberId = PepperMemory.memberId;
        int rewardType = reward.getType();
        int rewardId = reward.getId();
        String flowCode = PepperMemory.flow.getCode();
        String pepperName = PepperMemory.pepperName;

        new GiveReward(accessToken, memberId, rewardType, rewardId, flowCode, pepperName, new GiveReward.GiveRewardCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, String message) {

                // 通信成功・失敗確認
                if (response.isSuccess) {

                    if (!message.equals("")) {
                        // SMS送信へ
                        sendMessage();
                    }
                } else {
                    // 何らかのエラーがあった場合
                    activity.changeFragment(new ErrorFragment());
                }
            }
        }).excute();
    }

    // SMS送信
    private void sendMessage() {

        String accessToken = PepperMemory.accessToken;
        int memberId = PepperMemory.memberId;

        new SendMessage(accessToken, memberId, smsText, new SendMessage.SendMessageCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, boolean isSuccess) {

                // 通信成功・失敗確認
                if (response.isSuccess) {

                    // 送信失敗回数リセット
                    activity.timeOfSend = 0;

                    if (isSuccess) {
                        // 送信成功画面へ遷移
                        activity.changeFragment(new SmsSendSuccessFragment());
                    }
                } else {

                    // 送信失敗回数ふやす
                    activity.timeOfSend ++;
                    // 503エラー時
                    if (response.status == 503) {
                        activity.timeOfSend = activity.limitOfSend;
                    }

                    // 送信失敗画面へ遷移
                    activity.changeFragment(new SmsSendFailureFragment());
                }
            }
        }).execute();
    }
}
