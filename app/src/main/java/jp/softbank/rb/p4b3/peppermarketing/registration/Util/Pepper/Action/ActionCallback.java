package jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action;

public interface ActionCallback {
    void onSuccess();
    void onError(String errorMessage);
    void onCancel();
}
