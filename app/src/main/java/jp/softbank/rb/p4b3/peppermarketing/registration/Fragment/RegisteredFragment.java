package jp.softbank.rb.p4b3.peppermarketing.registration.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.softbank.rb.p4b3.peppermarketing.registration.Fragment.MarketingFragment;
import jp.softbank.rb.p4b3.peppermarketing.registration.R;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.PepperUtil;

public class RegisteredFragment extends MarketingFragment {

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_registered, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // 発話中なら発話中断
        if (say != null && say.isWorking()) {
            say.cancel();
        }

        // 発話してアプリを終了
        say = new Say(qiContext, getString(R.string.already_registered), true, new ActionCallback() {
            @Override
            public void onSuccess() {
                PepperUtil.finishApplication(activity);
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }
}
