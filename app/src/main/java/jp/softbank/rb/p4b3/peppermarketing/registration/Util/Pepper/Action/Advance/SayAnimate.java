package jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.Advance;

import android.util.Log;

import com.aldebaran.qi.sdk.QiContext;

import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.Basic.Animate;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper.Action.Basic.Say;

public class SayAnimate extends Say {

    private final String TAG = "SayAnimate";

    private QiContext qiContext;
    private String text;
    private int motionId;
    private ActionCallback callback;

    private Say say;
    private Animate animate;

    private int count = 0;
    private boolean isError = false;
    private boolean isCancel = false;

    boolean isWorking;

    public SayAnimate(QiContext qiContext, String text, int motionId, ActionCallback callback) {
        super(qiContext, text, false, callback);
        this.qiContext = qiContext;
        this.text = text;
        this.motionId = motionId;
        this.callback = callback;
    }

    @Override
    public void execute() {

        isWorking = true;

        ActionCallback myCallback = new ActionCallback() {
            @Override
            public void onSuccess() {

                count ++;

                if (count >= 2) {

                    isWorking = false;

                    if (callback != null) {

                        if (isError) {
                            Log.d(TAG, "error");
                            callback.onError("");
                        } else if (isCancel) {
                            Log.d(TAG, "cancel");
                            callback.onCancel();
                        } else {
                            Log.d(TAG, "succsess");
                            callback.onSuccess();
                        }
                    }
                }
            }

            @Override
            public void onError(String errorMessage) {

                isError = true;

                count ++;

                if (count >= 2) {

                    isWorking = false;

                    if (callback != null) {
                        Log.d(TAG, "error");
                        callback.onError(errorMessage);
                    }
                }
            }

            @Override
            public void onCancel() {

                isCancel = true;

                count ++;

                if (count >= 2) {

                    isWorking = false;

                    if (callback != null) {

                        if (isError) {
                            Log.d(TAG, "error");
                            callback.onError("");
                        } else {
                            Log.d(TAG, "cancel");
                            callback.onCancel();
                        }
                    }
                }
            }
        };

        say = new Say(qiContext, text, false, myCallback);

        animate = new Animate(qiContext, motionId, myCallback);

        say.execute();
        animate.execute();
    }

    @Override
    public void cancel() {
        if (say != null && animate != null) {
            say.cancel();
            animate.cancel();
        }
    }

    @Override
    public boolean isWorking() {
        return isWorking;
    }
}
