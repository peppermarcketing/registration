package jp.softbank.rb.p4b3.peppermarketing.registration.Util.Api;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.registration.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Logutil;

public class SendMessage {

    private final String TAG = this.getClass().getSimpleName();

    private String accessToken;
    private int memberId;
    private String smsText;
    private SendMessageCallback callback;

    public SendMessage(String accessToken, int memberId, String smsText, SendMessageCallback callback) {
        this.accessToken = accessToken;
        this.memberId = memberId;
        this.smsText = smsText;
        this.callback = callback;
    }

    // SMS送信
    public void execute() {

        Logutil.log("API_開始_SMS送信処理");

        // SMS送信
        final String url = "https://rap-vm.softbankrobotics.com/portal/pmkt/api3.php/message/send";

        // テスト用
        // final String url = "http://172.20.10.14/marketing/sms";

        // POSTのヘッダー情報付加
        ArrayList<HttpTask.Param> headers = new ArrayList<>();
        headers.add(new HttpTask.Param("Content-Type", "application/x-www-form-urlencoded"));

        // POSTパラメータ
        String param = "access_token=" + accessToken + "&" +
                "member_id=" + String.valueOf(memberId) + "&" +
                "sms_message=" + smsText;

        // 引数(URL,パラメータ,ヘッダー)
        final HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                param,
                headers
        );

        // 通信前処理、通信後処理を設定
        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    Logutil.log("API_終了_SMS送信処理");

                    // 通信成功
                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    // パース
                    boolean isSuccess = isSuccess(resultJson);
                    callback.onFinish(response, isSuccess);

                } else {

                    Logutil.log("API_エラー_SMS送信処理");

                    Log.e(TAG, response.errorMessage);
                    callback.onFinish(response, false);
                }
            }

            @Override
            public void onPre() { }
        }, true).execute(request);
    }

    // jsonから成功したかどうかを取得
    private boolean isSuccess(String strJson) {

        boolean success = false;

        try {
            JSONObject rootJson = new JSONObject(strJson);
            // keyで情報を取得
            success = rootJson.getBoolean("success");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return success;
    }

    public interface SendMessageCallback {
        void onFinish(HttpTask.HttpResponse response, boolean isSuccess);
    }
}
