package jp.softbank.rb.p4b3.peppermarketing.registration.Util;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.registration.Model.Flow;
import jp.softbank.rb.p4b3.peppermarketing.registration.Model.Reward;
import jp.softbank.rb.p4b3.peppermarketing.registration.Model.Term;


public class PepperMemory {

    // 設定項目
    public static final String robotToken = "3ngVT9sVCvMwNkFtnVm7dJkKzp7zffPg";
    public static String userId = "";
    public static String password = "";
    public static final String pepperName = "Pepperの名前";

    // パスワード
    public static final String adminPassword = "9999";

    // ぷリファレンスファイル名
    public static final String preferenceName = "pepper_marketing_p4b3_preference";

    // アプリケーションID
    public static final String appIdRegist = "biz_sbr_pmkt_registration";
    public static final String appIdPromo = "biz_sbr_pmkt_storepromotion";

    // グローバル変数
    public static String accessToken = "";   // アクセストークン
    public static Flow flow;                 // 今のフロー
    public static ArrayList<Reward> rewards; // リワードのリスト
    public static Term term;                 // 規約情報
    public static Reward selectedReward;     // (店舗配信用、選択されたリワード)
    public static int memberId = -1;         // 会員番号
    public static String smsText = "";       // SMSメッセージ保存
}
