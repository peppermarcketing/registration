package jp.softbank.rb.p4b3.peppermarketing.registration.Util.Pepper;

public interface FutureInterface {
    void onSuccess();
    void onError(String error);
}
