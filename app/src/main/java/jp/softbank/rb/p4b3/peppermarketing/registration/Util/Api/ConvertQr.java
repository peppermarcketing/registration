package jp.softbank.rb.p4b3.peppermarketing.registration.Util.Api;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.registration.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Logutil;


public class ConvertQr {

    private final String TAG = this.getClass().getSimpleName();

    private String accessToken;
    private String qrText;
    private ConvertQrCallback callback;

    public ConvertQr(String accessToken, String qrText, ConvertQrCallback callback) {
        this.accessToken = accessToken;
        this.qrText = qrText;
        this.callback = callback;
    }

    // バーコード変換処理
    public void excute() {

        Log.d(TAG, "read text is: " + qrText);
        Logutil.log("API_開始_バーコード変改処理");

        final String url = "https://rap-vm.softbankrobotics.com/portal/pmkt/api3.php/barcode/convert";

        // POSTのヘッダー情報付加
        ArrayList<HttpTask.Param> headers = new ArrayList<>();
        headers.add(new HttpTask.Param("Content-Type", "application/x-www-form-urlencoded"));

        // POSTパラメータ
        String param = "access_token=" + accessToken + "&" +
                "barcode_digits=" + qrText;

        // 引数(URL,パラメータ,ヘッダー)
        final HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                param,
                headers
        );

        // 通信前処理、通信後処理を設定
        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    Logutil.log("API_終了_バーコード変改処理");

                    // 通信成功
                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    // パース
                    int barcodeType = getBarcodeId(resultJson);
                    // barcodeTypeで処理を分ける
                    if (barcodeType == BarcodeType.MEMBERSHIP_CARD) {
                        // 会員証タイプ
                        int memberId = getMemberId(resultJson);
                        callback.onFinish(response, barcodeType, memberId, -1, -1);

                    } else if (barcodeType == BarcodeType.REWARD_DELVERY) {
                        // リワード配信QRタイプ
                        int rewardType = getRewardType(resultJson);
                        int rewardId = getRewardId(resultJson);
                        callback.onFinish(response, barcodeType, -1, rewardType, rewardId);
                    }

                } else {

                    Logutil.log("API_エラー_バーコード変改処理");

                    Log.e(TAG, response.errorMessage);
                    callback.onFinish(response, -1, -1, -1, -1);
                }
            }

            @Override
            public void onPre() { }
        }, true).execute(request);
    }


    // jsonからバーコードタイプを取得
    private static int getBarcodeId(String strJson) {

        int barcodeType = -1;

        try {

            JSONObject rootJson = new JSONObject(strJson);
            barcodeType = rootJson.getInt("barcode_type");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return barcodeType;
    }

    // jsonからメンバーIDを取得
    private static int getMemberId(String strJson) {

        int memberId = -1;

        try {

            JSONObject rootJson = new JSONObject(strJson);
            memberId = rootJson.getInt("member_id");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return memberId;
    }

    // jsonからリワードタイプを取得
    private static int getRewardType(String strJson) {

        int rewardType = -1;

        try {

            JSONObject rootJson = new JSONObject(strJson);
            rewardType = rootJson.getInt("reward_type");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rewardType;
    }

    // jsonからリワードIDを取得
    private static int getRewardId(String strJson) {

        int rewardId = -1;

        try {

            JSONObject rootJson = new JSONObject(strJson);
            rewardId = rootJson.getInt("reward_id");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rewardId;
    }

    // バーコードタイプ
    static public class BarcodeType {
        public static final int MEMBERSHIP_CARD = 1;
        public static final int REWARD_DELVERY = 2;
    }

    public interface ConvertQrCallback {
        void onFinish(HttpTask.HttpResponse response, int barcodeType, int memberId, int rewardType, int rewardId);
    }
}
