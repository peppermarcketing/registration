package jp.softbank.rb.p4b3.peppermarketing.registration.Util.Api;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.registration.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Logutil;

public class GetAccessToken {

    private final String TAG = this.getClass().getSimpleName();

    private String userId;
    private String password;
    private String robotToken;
    private GetAccessTokenCallback callback;

    public GetAccessToken(String robotToken, String userId, String password, GetAccessTokenCallback callback) {
        this.userId = userId;
        this.password = password;
        this.robotToken = robotToken;
        this.callback = callback;
    }

    // アクセストークン取得 (ロボットトークン使用)
    // TODO (UUIDでの取得になった後は削除予定)
    public void execute() {

        Logutil.log("API_開始_アクセストークン取得処理");

        final String url = "https://rap-vm.softbankrobotics.com/portal/pmkt/api3.php/manager/access_token";

        String param = "id=" + userId + "&" +
                "password=" + password + "&" +
                "robot_token=" + robotToken;

        ArrayList<HttpTask.Param> headers = new ArrayList<>();
        headers.add(new HttpTask.Param("Content-Type", "application/x-www-form-urlencoded"));

        final HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                param,
                headers);

        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    Logutil.log("API_終了_アクセストークン取得処理");
                    Logutil.log("認証成功_" + userId + "/" + password);

                    String result = HttpTask.byte2string(response.result);
                    Log.d(TAG, result);

                    String accessToken = getAccessToken(result);

                    Logutil.log("アクセストークン_" + accessToken);

                    callback.onFinish(response, accessToken);
                    
                } else {

                    Logutil.log("API_エラー_アクセストークン取得処理");

                    Log.e(TAG, response.errorMessage);
                    callback.onFinish(response, null);
                }
            }

            @Override public void onPre() { }
        }, true).execute(request);
    }


    // jsonからアクセストークンを取得
    private static String getAccessToken(String strJson) {

        String accessToken = "";

        try {
            JSONObject rootJson = new JSONObject(strJson);
            accessToken = rootJson.getString("access_token");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return accessToken;
    }

    public interface GetAccessTokenCallback {
        void onFinish(HttpTask.HttpResponse response, String accessToken);
    }
}
