package jp.softbank.rb.p4b3.peppermarketing.registration.Util.Api;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.registration.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.registration.Util.Logutil;


public class UpdateMembership {

    private final String TAG = this.getClass().getSimpleName();

    private String accessToken;
    private int memberId;
    private int flowType;
    private String flowCode;
    private String pepperName;
    private UpdateMembershipCallback callback;

    public UpdateMembership(String accessToken, int memberId, int flowType, String flowCode, String pepperName, UpdateMembershipCallback callback) {
        this.accessToken = accessToken;
        this.memberId = memberId;
        this.flowType = flowType;
        this.flowCode = flowCode;
        this.pepperName = pepperName;
        this.callback = callback;
    }

    // 会員情報更新
    public void execute() {

        Logutil.log("API_開始_会員情報更新処理");

        // 会員情報更新
        final String url = "https://rap-vm.softbankrobotics.com/portal/pmkt/api3.php/member/update";

        // POSTのヘッダー情報付加
        ArrayList<HttpTask.Param> headers = new ArrayList<>();
        headers.add(new HttpTask.Param("Content-Type", "application/x-www-form-urlencoded"));

        // POSTパラメータ
        String param = "access_token=" + accessToken + "&" +
                "member_id=" + String.valueOf(memberId) + "&" +
                "flow_type=" + String.valueOf(flowType) + "&" +
                "flow_code=" + flowCode + "&" +
                "pepper_name=" + pepperName;

        // 引数(URL,パラメータ,ヘッダー)
        final HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                param,
                headers
        );

        // 通信前処理、通信後処理を設定
        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    Logutil.log("API_終了_会員情報更新処理");

                    // 通信成功
                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    // パース
                    int memberId_ = getMemberId(resultJson);
                    String message = getMessage(resultJson);
                    callback.onFinish(response, memberId_, message);

                } else {

                    Logutil.log("API_エラー_会員情報更新処理");

                    // 通信失敗
                    Log.d(TAG, "error: " + response.errorMessage);
                    callback.onFinish(response, -1, null);
                }
            }

            @Override
            public void onPre() { }
        }, true).execute(request);
    }


    // jsonからメンバーIDを取得
    private int getMemberId(String strJson) {

        int memberId = -1;

        try {
            JSONObject rootJson = new JSONObject(strJson);
            memberId = rootJson.getInt("member_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return memberId;
    }

    // jsonからURLなどのメッセージを取得
    private String getMessage(String strJson) {

        String message = "";

        try {
            JSONObject rootJson = new JSONObject(strJson);
            if (!rootJson.isNull("pincode")) {
                message = rootJson.getString("pincode");
            } else if (!rootJson.isNull("registration_shorturl")) {
                message = rootJson.getString("registration_shorturl");
            } else if (!rootJson.isNull("blank_emailaddress")) {
                message = rootJson.getString("blank_emailaddress");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return message;
    }

    public interface UpdateMembershipCallback {
        void onFinish(HttpTask.HttpResponse response, int memberId, String message);
    }
}
